﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("References")]
    public Transform cam;
    public Vector3 attackPoint;

    [Header("Throwing")]
    public KeyCode throwKey = KeyCode.Mouse0;
    public float throwForce;
    public float throwUpwardForce;

    GameObject projectile;

    public bool holding;

    private void Start()
    {
        holding = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(throwKey) && !holding)
            Catch();
        else if (Input.GetKey(throwKey) && holding)
            Hold();
        if (Input.GetKeyUp(throwKey) && holding)
            Throw();
    }
    private void Catch()
    {
        RaycastHit hit;
        attackPoint = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 5f));

        if (Physics.Raycast(attackPoint, cam.forward, out hit, 10f))
        {
            if (hit.transform.gameObject.GetComponent<Rigidbody>() != null)
            {
                projectile = hit.transform.gameObject;
                holding = true;
            }
        }
    }
    private void Hold()
    {
        attackPoint = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 5f));
        projectile.transform.position = new Vector3(attackPoint.x, attackPoint.y, attackPoint.z + 1);
    }
    private void Throw()
    {
        Rigidbody projectileRb = projectile.GetComponent<Rigidbody>();

        Vector3 forceDirection = (new Vector3(transform.position.x, transform.position.y, transform.position.z + 15) - attackPoint).normalized;

        Vector3 forceToAdd = forceDirection * throwForce + transform.up * throwUpwardForce;

        projectileRb.AddForce(forceToAdd, ForceMode.Impulse);

        projectile = null;
        holding = false;
    }

}